package gosmtpd

import "log"

type MailAddress interface {
	Email() string
	Hostname() string
}

type Envelope interface {
	AddRecipient(rcpt MailAddress) error
	BeginData() error
	Write(line []byte) error
	Close() error
}

type BasicEnvelope struct {
	rcpts  []MailAddress
	sender MailAddress
}

func (e *BasicEnvelope) AddRecipient(rcpt MailAddress) error {
	//TODO
	//CHECK IF EMAIL IS RIGHT
	e.rcpts = append(e.rcpts, rcpt)
	return nil
}

func (e *BasicEnvelope) BeginData() error {
	if len(e.rcpts) == 0 {
		return SMTPError("554 5.5.1 Error: no valid recipients")
	}
	return nil
}

func (e *BasicEnvelope) Write(line []byte) error {
	log.Printf("Line: %q", string(line))
	return nil
}

func (e *BasicEnvelope) Close() error {
	return nil
}
