package gosmtpd

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"log"
	"net"
	"regexp"
	"strings"
	"time"
	"unicode"
)

type session struct {
	server     *Server
	conn       net.Conn
	remoteIP   string
	remoteHost string
	remoteName string
	br         *bufio.Reader
	bw         *bufio.Writer
	//env        Envelope
	helloType string         //whether its using esmtp or not
	env       *BasicEnvelope //stores sender and receipts addresses
	//helloHost  string
}

var (
	mailFromRE = regexp.MustCompile(`[Ff][Rr][Oo][Mm]:<(.*)>`) //MAIL FROM: <email address>
	rcptToRe   = regexp.MustCompile(`[Tt][Oo]:<(.+)>`)
)

var (
	OK_RESPONSE       = "250 2.1.0 Ok"
	NEED_SENDER_MAIL  = "503 5.5.1 Error: need MAIL command"
	BAD_EMAIL_ADDR    = "501 5.1.7 Bad sender address syntax"
	NEED_RCPT_COMMAND = "503 5.5.1 Error: need RCPT command"
	DATA_OK           = "250 2.0.0 Ok: queued"
)

func (server *Server) NewSession(conn net.Conn) (s *session, err error) {
	fmt.Println("this caled")
	s = &session{
		server: server,
		conn:   conn,
		br:     bufio.NewReader(conn),
		bw:     bufio.NewWriter(conn),
	}
	return
}

func (s *session) HandleConnection() {
	defer s.conn.Close()

	s.remoteIP, _, _ = net.SplitHostPort(s.conn.RemoteAddr().String())
	names, err := net.LookupAddr(s.remoteIP)
	if err == nil && len(names) > 0 {
		s.remoteHost = names[0]
	} else {
		s.remoteHost = "unknown"
	}

	fmt.Println("Connection established from: %v.", s.remoteIP)

	//greet here
	s.sendf("220 %s %s SMTP service ready\r\n", s.server.Hostname, s.server.Appname)
	for {
		line, err := s.br.ReadSlice('\n')
		n := len(line)

		if err != nil {
			//if err != nil || n == 0 {
			fmt.Println("Error occured while reading: ", err)
			s.conn.Close()
			return
		}

		if err := cmdLine(line).checkValid(); err != nil {
			s.sendlinef("500 %v", err)
			continue
		}

		//exclude new line characters
		command, args := ParseCommand(string(line[:n-2]))
		fmt.Println("Command: ", command)
		fmt.Println("Line: ", string(line))

		switch command {

		//Greeting server
		case "EHLO", "HELO":

			s.HeloHandler(command, args)

		//getting receipt's email address
		case "MAIL":

			s.MailFromHandler(args)

		case "RCPT":

			s.RcptHandler(cmdLine(string(line)))

		case "DATA":
			s.DataHandler()

		case "RSET":
			s.env = nil
			s.sendlinef(OK_RESPONSE)

		case "NOOP":
			s.sendlinef(OK_RESPONSE)

		case "QUIT":
			s.sendlinef("221 2.0.0 Bye")
			return

		default:
			//s.Writef("500 Syntax error, command unrecognized\n")
			s.sendlinef("502 5.5.2 error, command unrecognized\n")
		}
	}
	fmt.Println("Connection from %v is closed.", s.remoteIP)
}

/****************
*****************
 HELPER FUNCTIONS
*****************
****************/

func ParseCommand(line string) (command string, args string) {
	if i := strings.Index(line, " "); i != -1 {
		command = strings.ToUpper(line[:i])
		args = strings.TrimSpace(line[i+1 : len(line)])
	} else {
		command = strings.ToUpper(line)
		args = ""
	}
	return command, args
}

//func (c cmdLine)
func (s *session) errorf(format string, args ...interface{}) {
	log.Printf("Client error: "+format, args...)
}

func (s *session) Writef(format string, args ...interface{}) {
	_, err := s.conn.Write([]byte(fmt.Sprintf(format, args...)))
	if err != nil {
		fmt.Println("Error: ", err)
	}
}

func (s *session) sendf(format string, args ...interface{}) {
	if s.server.WriteTimeout != 0 {
		s.conn.SetWriteDeadline(time.Now().Add(s.server.WriteTimeout))
	}
	fmt.Fprintf(s.bw, format, args...)
	s.bw.Flush()
}

func (s *session) sendlinef(format string, args ...interface{}) {
	s.sendf(format+"\r\n", args...)
}

func (s *session) handleError(err error) {
	if se, ok := err.(SMTPError); ok {
		s.sendlinef("%s", se)
		return
	}
	log.Printf("Error: %s", err)
	s.env = nil
}

//email address as a type and it's functions
type addrString string

func (a addrString) Email() string {
	return string(a)
}

func (a addrString) Hostname() string {
	e := string(a)
	if idx := strings.Index(e, "@"); idx != -1 {
		return strings.ToLower(e[idx+1:])
	}
	return ""
}

//command in line as a type and it's functions
type cmdLine string

//Parses command verb from given line
func (c cmdLine) Command() (command string) {
	s := string(c)
	if i := strings.Index(s, " "); i != -1 {
		command = strings.ToUpper(s[:i])
	} else {
		command = strings.ToUpper(s)
	}
	return
}

//Parses arguments from given line
func (c cmdLine) Arg() string {
	s := string(c)
	if i := strings.Index(s, " "); i != -1 {
		return strings.TrimRightFunc(s[i+1:len(s)-2], unicode.IsSpace)
	}
	return ""
}

func (c cmdLine) checkValid() error {
	s := string(c)
	//error is raised if it doesn't end with
	if !strings.HasSuffix(s, "\r\n") {
		return errors.New(`line doesn't end in \r\n`)
	}

	//these commands are not supposed to have arguments
	command, arg := ParseCommand(s)
	switch command {
	case "RSET", "DATA", "QUIT":
		if arg != "" {
			return errors.New("unexpected argument")
		}
	}
	return nil
}

type SMTPError string

func (e SMTPError) Error() string {
	return string(e)
}

/*******************
*******************
	HANDLERS
*******************
*******************/

//when client entered EHLO or HELO command
func (s *session) HeloHandler(command, args string) {
	s.remoteName = args
	s.helloType = command
	////!!!!!Careful here might be flushed!!!!
	//s.sendf("250 %s greets %s\n", s.server.Hostname, s.remoteName)
	fmt.Fprintf(s.bw, "250-%s greets %s\n", s.server.Hostname, s.remoteName)
	extensions := []string{}
	if s.server.AuthType {
		extensions = append(extensions, "250-AUTH PLAIN")
	}
	extensions = append(extensions, "250-PIPELINING",
		"250-SIZE 10240000",
		"250-ENHANCEDSTATUSCODES",
		"250-8BITMIME",
		"250 DSN")
	for _, ext := range extensions {
		fmt.Fprintf(s.bw, "%s\r\n", ext)
	}
	s.bw.Flush()
}

func (s *session) MailFromHandler(arg string) {

	//TODO
	//check whether host is right
	//reject email if there is something wrong with sender

	mailFrom := mailFromRE.FindStringSubmatch(arg)
	if mailFrom == nil {
		s.sendf("501 5.1.7 Bad sender address syntax (invalid FROM parameter)\n")
	} else {
		fmt.Println(mailFrom)
	}

	if s.env != nil {
		if s.env.sender != nil {
			s.sendlinef("503 5.5.1 Error: nested MAIL command")
			return
		}
	}

	/*
		if s.env.sender != nil {
			s.sendlinef("503 5.5.1 Error: nested MAIL command")
			return
		}
	*/

	s.env = new(BasicEnvelope)
	s.env.sender = addrString(mailFrom[1])

	log.Printf("Mail from: %q", arg)
	fmt.Println("ENV DEBUG: ", s.env)
	s.sendlinef(OK_RESPONSE)
}

func (s *session) RcptHandler(line cmdLine) {
	if s.env != nil {
		if s.env.sender == nil {
			s.sendlinef(NEED_SENDER_MAIL)
			return
		}
	}

	arg := line.Arg()
	rcpt_addr := rcptToRe.FindStringSubmatch(arg)
	if rcpt_addr == nil {
		log.Printf("bad RCPT Address: %q", arg)
		s.sendlinef(BAD_EMAIL_ADDR)
		return
	}
	//.......
	//TODO
	//FIX THIS

	err := s.env.AddRecipient(addrString(rcpt_addr[1]))
	if err != nil {
		s.sendlinef("500 bad recipient")
		return
	}

	s.sendlinef(OK_RESPONSE)
}

func (s *session) DataHandler() {
	if s.env == nil {
		s.sendlinef(NEED_RCPT_COMMAND)
		return
	}

	if err := s.env.BeginData(); err != nil {
		s.handleError(err)
		return
	}
	s.sendlinef("354 Go ahead")
	for {
		sl, err := s.br.ReadSlice('\n')
		if err != nil {
			s.errorf("read error: %v", err)
			return
		}

		if bytes.Equal(sl, []byte(".\r\n")) {
			break
		}
		if sl[0] == '.' {
			sl = sl[1:]
		}
		err = s.env.Write(sl)
		if err != nil {
			s.sendlinef("Write error occured")
			return
		}
	}
	s.env.Close()
	s.sendlinef(DATA_OK)
	s.env = nil
}
