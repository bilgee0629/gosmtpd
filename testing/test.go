package main

import (
	"fmt"

	"gosmtpd"
)

func main() {
	err := gosmtpd.StartServer("", "SMTP go", "testhost", ":25")
	if err != nil {
		fmt.Println("Error: ", err)
	}
}
